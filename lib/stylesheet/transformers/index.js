// function(context, file) {
//   return '@import "'+ file + '";';
//   // use `~` to import from a node module
//   // i think gem assets still need relative path?
// },

module.exports = {
  require: require('./require'),
  requireTree: function() {
    console.log('stylesheet/transformers/requireTree', arguments);
  },
  requireDirectory: function() {
    console.log('stylesheet/transformers/requireDirectory', arguments);
  },
  requireSelf: function() {
    console.log('stylesheet/transformers/requireSelf', arguments);
  },
};
