var stylesheetParsers = require('./directive-parsers');
var transformers = require('./transformers');
var startOfCommentBlock = /^\/\*/;
var endOfCommentBlock = /\*\//;

module.exports = function(context, source) {
  var lineParseReducer = function(outputLines, line) {
    var requireMatches = stylesheetParsers.require(line);

    if (requireMatches) {
      if (startOfCommentBlock.exec(outputLines[outputLines.length - 1]))
        outputLines.pop()

      return outputLines.concat(transformers.require(context, requireMatches[3]));
    }

    var requireTreeMatches = stylesheetParsers.requireTree(line);
    if (requireTreeMatches) {
      if (startOfCommentBlock.exec(outputLines[outputLines.length - 1]))
        outputLines.pop()

      return outputLines.concat(transformers.requireTree(context, requireTreeMatches[3]));
    }

    var requireDirectoryMatches = stylesheetParsers.requireDirectory(line);
    if (requireDirectoryMatches) {
      if (startOfCommentBlock.exec(outputLines[outputLines.length - 1]))
        outputLines.pop()

      return outputLines.concat(transformers.requireDirectory(context, requireDirectoryMatches[3]));
    }

    var requireSelfMatches = stylesheetParsers.requireSelf(line);
    if (requireSelfMatches) {
      if (startOfCommentBlock.exec(outputLines[outputLines.length - 1]))
        outputLines.pop()

      return outputLines.concat(transformers.requireSelf(source));
    }

    if (endOfCommentBlock.exec(line)) {
      if (/@import/.exec(outputLines[outputLines.length - 1]))
        return outputLines;
    }


    return outputLines.concat(line);
  };

  console.log('1==============================================================');
  console.log('1==============================================================');
  console.log('1==============================================================');
  console.log(source);
  var output = source.split('\n').reduce(lineParseReducer, []).join('\n');
  console.log('2==============================================================');
  console.log(output);
  console.log('3==============================================================');
  console.log('3==============================================================');
  console.log('3==============================================================');
  return output;
}
