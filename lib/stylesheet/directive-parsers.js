var regex = {
  require: /(\*=)\s*(require)\s+(\S+)/,
  requireTree: /(\*=)\s*(require_tree)\s+(\S+)/,
  requireDirectory: /(\*=)\s*(require_directory)\s+(\S+)/,
  requireSelf: /(\*=)\s*(require_self)/,
};

function parseDirective(directive) {
  return function parsingDirective(line) {
    return directive.exec(line);
  };
}

module.exports = {
  require: parseDirective(regex.require),
  requireTree: parseDirective(regex.requireTree),
  requireDirectory: parseDirective(regex.requireDirectory),
  requireSelf: parseDirective(regex.requireSelf),
};
